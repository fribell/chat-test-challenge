# Test challenge for an Interview #

## Instructions: ##
I've been provided with source code in Android Studio, and they are looking for fixing the run/compile time errors to be fixed with the best engineering approach. There are parts of the code that are not complete and it's free to add the libraries that you want.

**First part:**

- First View will include a given image and login details. You need to enter a name and this will be your name for the rest of the app.
- These names are already reserved: "Carrie - Anthony - Eleanor - Rodney - Oliva - Merve or Lily"

**Second part:**

- The second view you will display the chat.
- You will have to fetch the content of "https://s3-eu-
west-1.amazonaws.com/rocket-interview/chat.json" and display them according to some wireframe.
- When you type in some text to current chat your chat bubble should appear like dark grey one as shown at the design.
- Logout button on the top right
- If the user doesn't logout, the App should show the chat screen directly
without login page, whenever it launches.


## Summary of changes that I've done in the app: ##
**Setup project:**

- Update buildToolsVersion and compileSdkVersion to 25
- Update targetSdkVersion to 25
- Update android libraries
- Setup gradle to create a release (The password is still in the gradle file but it should be sent in the CI process) and debug build. You have 2 different packages name so you can have both at the same time.
- Obfuscate the release app and set proguard rules to make it work

**Architecture of the project:**

- I've implemented MVP
- Reorganise folders and files
- Use android databinding to inject view elements in the activities
- Add retrofit library to communicate with the APIs
- Add repositories to be able to get the information from different places(database, other API,...) in the future.
- Save user in the sharedPreferences, it was the easy way to don't add another library like realm or dbflow for database purpose.
- I've added glide library to load the images and make the circular effect of the profile

**UI:**

- The login layout wasn't working properly in landscape mode so I've added everything in the scrollview to be able to login in landscape mode in some devices with small screen.
- Handle orientation change in the chat activity, to keep the messages added for the user.
- I've changed listView for recyclerview because is more flexible
- I've changed the adapter to fit recyclerviewadapter
- I've added databinding like I said before
- I've added snackbar to show error messages in the chat activity
- I've added loading spinner in the chat activity when we are asking to the server for the messages
- Login editText use capital letters
- Chat editText use capital letters
- I've moved around some objects in the chatactivity just to look better
- I'm setting different bubbles style using databinding (just changing 2 attributes) instead of doing it in the adapter.

I've implemented some unit test.
I've run the Lint tool to find some layout/code issues. There are still some warnings.
I've done all the TODOs items.
I've tested the app with my Nexus 5X (7.1.2) and also with Emulator API 23 and 17, In tablets should work and scale everything ok.