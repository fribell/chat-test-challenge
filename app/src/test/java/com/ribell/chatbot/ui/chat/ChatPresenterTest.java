package com.ribell.chatbot.ui.chat;

import com.ribell.chatbot.interfaces.OnCompletionListener;
import com.ribell.chatbot.repositories.MessageRepository;
import com.ribell.chatbot.repositories.SharedPreferencesRepository;
import com.ribell.chatbot.ui.chat.ChatMvp;
import com.ribell.chatbot.ui.chat.ChatPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

/**
 * Created by ferranribell on 15/02/2017.
 */
public class ChatPresenterTest {
    @Mock
    ChatMvp.View view;

    @Mock
    MessageRepository messageRepository;

    @Mock
    SharedPreferencesRepository sharedPreferencesRepository;

    private ChatPresenter presenter;

    @Before
    public void create() {
        MockitoAnnotations.initMocks(this);

        presenter = new ChatPresenter(messageRepository, sharedPreferencesRepository);
        presenter.attachView(view);
    }

    @Test
    public void loadValidMessages() throws Exception {
        presenter.loadMessages();
        verify(messageRepository).getMessages(any(OnCompletionListener.class));
    }

    @Test
    public void logoutSuccess() {
        presenter.logout();
        verify(view).moveToLoginActivity();
    }
}