package com.ribell.chatbot.ui.login;

import com.ribell.chatbot.repositories.SharedPreferencesRepository;
import com.ribell.chatbot.ui.login.LoginMvp;
import com.ribell.chatbot.ui.login.LoginPresenter;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

/**
 * Created by ferranribell on 15/02/2017.
 */
public class LoginPresenterTest {


    @Mock
    LoginMvp.View view;

    @Mock
    SharedPreferencesRepository sharedPreferencesRepository;

    private LoginPresenter presenter;

    @Before
    public void create() {
        MockitoAnnotations.initMocks(this);

        presenter = new LoginPresenter(sharedPreferencesRepository);
        presenter.attachView(view);
    }

    @Test
    public void validateInvalidNameAndSurname() throws Exception {
        String name = "tet";

        presenter.attemptLogin(name);

        verify(view).resetError();
        verify(view).nameInvalidError();
    }

    @Test
    public void validateExistingName() throws Exception {
        String name = "Lily test";

        presenter.attemptLogin(name);

        verify(view).resetError();
        verify(view).nameReservedError();
    }

    @Test
    public void loginSuccess() throws Exception {
        String name = "Lilysss test";

        presenter.attemptLogin(name);

        verify(view).resetError();
        verify(view).loginSuccess();
    }
}