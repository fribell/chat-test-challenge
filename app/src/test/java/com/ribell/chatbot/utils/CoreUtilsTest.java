package com.ribell.chatbot.utils;

import com.ribell.chatbot.utils.CoreUtils;

import junit.framework.Assert;

import org.junit.Test;

/**
 * Created by ferranribell on 15/02/2017.
 */
public class CoreUtilsTest {

    @Test
    public void validateNameAndSurname() {
        String name = "";
        Assert.assertFalse(CoreUtils.isNameValid(name));

        name = "Ferran";
        Assert.assertFalse(CoreUtils.isNameValid(name));

        name = "Ferran ";
        Assert.assertFalse(CoreUtils.isNameValid(name));

        name = " Ferran";
        Assert.assertFalse(CoreUtils.isNameValid(name));

        name = " Ferran ";
        Assert.assertFalse(CoreUtils.isNameValid(name));

        name = "Ferran Ribell";
        Assert.assertTrue(CoreUtils.isNameValid(name));

        name = " Ferran Ribell ";
        Assert.assertTrue(CoreUtils.isNameValid(name));

        name = "Jhonny-Test Test";
        Assert.assertTrue(CoreUtils.isNameValid(name));
    }

    @Test
    public void checkIfIsReservedName() {
        String name = "Carrie";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Anthony Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Eleanor Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Rodney Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Oliva Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Merve Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Lily Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "lily Test";
        Assert.assertTrue(CoreUtils.isReservedName(name));

        name = "Lilys Test";
        Assert.assertFalse(CoreUtils.isReservedName(name));

        name = "Ferran Ribell";
        Assert.assertFalse(CoreUtils.isReservedName(name));
    }
}