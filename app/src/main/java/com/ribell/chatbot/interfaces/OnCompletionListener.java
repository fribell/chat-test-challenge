package com.ribell.chatbot.interfaces;

public interface OnCompletionListener<T> {

    void onCompletionSuccess(T object);

    void onCompletionError(int error);

}
