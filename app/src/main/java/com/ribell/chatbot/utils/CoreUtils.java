package com.ribell.chatbot.utils;

import com.ribell.chatbot.model.User;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CoreUtils {
    public static boolean isNameValid(String name) {
        String regExp = "[A-Za-z]+\\s+['A-Za-z]+";
        Pattern pattern = Pattern.compile(regExp, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(name);

        return matcher.find();
    }

    public static boolean isReservedName(String name) {
        // we should get those names from the API to check if an user with that name already exist
        String[] reservedNames = {"Carrie", "Anthony", "Eleanor", "Rodney", "Oliva", "Merve", "Lily"};

        User user = new User(name);
        for (String reservedName : reservedNames) {
            if (user.getName().equalsIgnoreCase(reservedName)) {
                return true;
            }
        }

        return false;
    }
}
