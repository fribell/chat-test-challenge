package com.ribell.chatbot.net.responses;

import com.google.gson.annotations.SerializedName;
import com.ribell.chatbot.model.ChatMessage;

import java.util.ArrayList;

public class ChatsResponse {

    @SerializedName("chats")
    private ArrayList<ChatMessage> chats;

    public ArrayList<ChatMessage> getMessages() {

        return chats;
    }
}
