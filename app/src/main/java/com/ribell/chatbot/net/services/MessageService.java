package com.ribell.chatbot.net.services;

import com.ribell.chatbot.net.responses.ChatsResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface MessageService {
    @GET("/rocket-interview/chat.json")
    Call<ChatsResponse> getMessages();
}
