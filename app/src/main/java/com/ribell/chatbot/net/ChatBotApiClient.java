package com.ribell.chatbot.net;

import com.ribell.chatbot.BuildConfig;
import com.ribell.chatbot.net.services.MessageService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ChatBotApiClient {

    private final Retrofit retrofit;

    // We use that API in only one place if we need to use it in more places
    // then we can add it as singleton to reuse the same object all the time or other options
    public ChatBotApiClient() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public MessageService getMessageService() {
        return retrofit.create(MessageService.class);
    }
}
