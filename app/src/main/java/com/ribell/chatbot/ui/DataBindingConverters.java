package com.ribell.chatbot.ui;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.ribell.chatbot.R;

public class DataBindingConverters {

    @BindingAdapter({"bind:userImage"})
    public static void loadUserImage(final ImageView imageView, String url) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(imageView.getContext()).load(url).asBitmap().centerCrop().into(new BitmapImageViewTarget(imageView) {
                @Override
                protected void setResource(Bitmap resource) {
                    RoundedBitmapDrawable circularBitmapDrawable =
                            RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), resource);
                    circularBitmapDrawable.setCircular(true);
                    imageView.setImageDrawable(circularBitmapDrawable);
                }

                @Override
                public void onLoadFailed(Exception e, Drawable errorDrawable) {
                    setPlaceholder(imageView);
                    super.onLoadFailed(e, errorDrawable);
                }
            });
        } else {
            setPlaceholder(imageView);
        }
    }

    private static void setPlaceholder(ImageView imageView) {
        RoundedBitmapDrawable circularBitmapDrawable =
                RoundedBitmapDrawableFactory.create(imageView.getContext().getResources(), BitmapFactory.decodeResource(imageView.getContext().getResources(), R.mipmap.ic_launcher));
        circularBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(circularBitmapDrawable);
    }

    @BindingAdapter("android:layout_alignParentEnd")
    public static void setAlignParentEnd(View view, boolean alignParentEnd) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                view.getLayoutParams()
        );

        if (alignParentEnd) {
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        }

        view.setLayoutParams(layoutParams);
    }
}
