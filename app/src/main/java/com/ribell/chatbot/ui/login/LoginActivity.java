package com.ribell.chatbot.ui.login;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;

import com.ribell.chatbot.R;
import com.ribell.chatbot.databinding.ActivityLoginBinding;
import com.ribell.chatbot.repositories.SharedPreferencesRepositoryImpl;
import com.ribell.chatbot.ui.chat.ChatActivity;

public class LoginActivity extends AppCompatActivity implements LoginMvp.View {

    // UI references.
    private ActivityLoginBinding binding;
    private LoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);

        // We could implement dagger and then we can inject the repository directly to the presenter
        SharedPreferencesRepositoryImpl sharedPreferencesRepository =
                new SharedPreferencesRepositoryImpl(
                        getSharedPreferences(SharedPreferencesRepositoryImpl.TAG, MODE_PRIVATE));

        presenter = new LoginPresenter(sharedPreferencesRepository);
        presenter.attachView(this);
        if (sharedPreferencesRepository.isLoggedIn()) {
            loginSuccess();
        } else {
            setLoginButton();
        }
    }

    private void setLoginButton() {
        binding.signInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.attemptLogin(binding.name.getText().toString());
            }
        });
    }

    @Override
    public void resetError() {
        binding.name.setError(null);
    }

    @Override
    public void nameEmptyError() {
        setNameError(R.string.error_field_required);
    }

    @Override
    public void nameInvalidError() {
        setNameError(R.string.error_invalid_name);
    }

    @Override
    public void nameReservedError() {
        setNameError(R.string.error_reserved_name);
    }

    @Override
    public void loginSuccess() {
        Intent intent = new Intent(this, ChatActivity.class);
        startActivity(intent);
        finish();
    }

    private void setNameError(@StringRes int errorId) {
        binding.name.setError(getString(errorId));
        binding.name.requestFocus();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}

