package com.ribell.chatbot.ui.login;

import com.ribell.chatbot.ui.core.BaseMvp;

public interface LoginMvp extends BaseMvp {
    interface View extends BaseView {
        void resetError();

        void nameEmptyError();

        void nameInvalidError();

        void nameReservedError();

        void loginSuccess();
    }

    interface Presenter extends BasePresenter<View> {
        void attemptLogin(String name);
    }
}
