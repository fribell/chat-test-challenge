package com.ribell.chatbot.ui.login;

import android.text.TextUtils;

import com.ribell.chatbot.model.User;
import com.ribell.chatbot.repositories.SharedPreferencesRepository;
import com.ribell.chatbot.ui.core.BasePresenter;
import com.ribell.chatbot.utils.CoreUtils;

class LoginPresenter extends BasePresenter<LoginMvp.View> implements LoginMvp.Presenter {

    private final SharedPreferencesRepository sharedPreferencesRepository;

    public LoginPresenter(SharedPreferencesRepository sharedPreferencesRepository) {
        this.sharedPreferencesRepository = sharedPreferencesRepository;
    }

    @Override
    public void attemptLogin(String name) {
        view.resetError();

        if (TextUtils.isEmpty(name)) {
            view.nameEmptyError();
        } else if (!CoreUtils.isNameValid(name)) {
            view.nameInvalidError();
        } else if (CoreUtils.isReservedName(name)) {
            view.nameReservedError();
        } else {
            saveUser(name);
            view.loginSuccess();
        }
    }

    private void saveUser(String name) {
        sharedPreferencesRepository.saveUser(new User(name));
    }

}
