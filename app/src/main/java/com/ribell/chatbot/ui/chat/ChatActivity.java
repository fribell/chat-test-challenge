package com.ribell.chatbot.ui.chat;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.ribell.chatbot.databinding.ActivityChatBinding;
import com.ribell.chatbot.R;
import com.ribell.chatbot.model.ChatMessage;
import com.ribell.chatbot.net.ChatBotApiClient;
import com.ribell.chatbot.repositories.MessageRepositoryApi;
import com.ribell.chatbot.repositories.SharedPreferencesRepositoryImpl;
import com.ribell.chatbot.ui.chat.adapters.ChatRecyclerViewAdapter;
import com.ribell.chatbot.ui.login.LoginActivity;

import java.util.ArrayList;


public class ChatActivity extends AppCompatActivity implements ChatMvp.View {

    private static final String MESSAGES_KEY = "MESSAGES_KEY";
    private ActivityChatBinding binding;
    private ChatPresenter presenter;
    private ChatRecyclerViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_chat);

        SharedPreferencesRepositoryImpl sharedPreferencesRepository =
                new SharedPreferencesRepositoryImpl(
                        getSharedPreferences(SharedPreferencesRepositoryImpl.TAG, MODE_PRIVATE));
        ChatBotApiClient apiClient = new ChatBotApiClient();
        MessageRepositoryApi repositoryAPI = new MessageRepositoryApi(apiClient);

        presenter = new ChatPresenter(repositoryAPI, sharedPreferencesRepository);
        presenter.attachView(this);

        if (savedInstanceState != null) {
            displayMessages((ArrayList<ChatMessage>) savedInstanceState.getSerializable(MESSAGES_KEY));
        } else {
            presenter.loadMessages();
        }

        setChatActionBar();
        setRefreshButton();
        setSendButton();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(MESSAGES_KEY, adapter.getMessages());
        super.onSaveInstanceState(outState);
    }

    private void setSendButton() {
        binding.sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.sendMessage(binding.message.getText().toString());
            }
        });
    }

    private void setRefreshButton() {
        binding.refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.loadMessages();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                presenter.logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setChatActionBar() {
        setTitle(R.string.chat_title_bar);
    }

    @Override
    public void displayMessages(ArrayList<ChatMessage> arrayOfMessages) {
        binding.progressBar.setVisibility(View.GONE);
        binding.noMessagesWrapper.setVisibility(View.GONE);

        binding.lvMessages.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        // We can use that to see the latest message or disable if we want to see the first one
        layoutManager.setStackFromEnd(true);
        binding.lvMessages.setLayoutManager(layoutManager);

        adapter = new ChatRecyclerViewAdapter(arrayOfMessages);
        binding.lvMessages.setAdapter(adapter);
    }

    @Override
    public void displayEmptyMessages() {
        binding.noMessagesWrapper.setVisibility(View.VISIBLE);
    }

    @Override
    public void internetConnectionError() {
        showSnackbarError(R.string.no_internet_connection);
    }

    @Override
    public void unknownError() {
        showSnackbarError(R.string.unknown_error);
    }

    @Override
    public void displayUserMessage(ChatMessage chatMessage) {
        adapter.addMessage(chatMessage);
        binding.lvMessages.scrollToPosition(adapter.getItemCount() - 1);
        binding.message.getText().clear();
    }

    @Override
    public void moveToLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showSnackbarError(@StringRes int message) {
        binding.progressBar.setVisibility(View.GONE);
        Snackbar.make(binding.getRoot(), message, Snackbar.LENGTH_INDEFINITE)
                .setAction(R.string.retry, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        binding.progressBar.setVisibility(View.VISIBLE);
                        presenter.loadMessages();
                    }
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}
