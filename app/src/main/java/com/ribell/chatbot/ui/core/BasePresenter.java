package com.ribell.chatbot.ui.core;

public class BasePresenter<V extends BaseMvp.BaseView> implements BaseMvp.BasePresenter<V> {

    protected V view;

    protected BasePresenter() {
    }

    protected boolean isViewAttached() {
        return view != null;
    }

    @Override
    public void attachView(V view) {
        this.view = view;
    }

    @Override
    public void detachView() {
        view = null;
    }

}
