package com.ribell.chatbot.ui.chat.adapters;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.ribell.chatbot.R;
import com.ribell.chatbot.databinding.ItemMessageBinding;
import com.ribell.chatbot.model.ChatMessage;

import java.util.ArrayList;

public class ChatRecyclerViewAdapter extends RecyclerView.Adapter<ChatRecyclerViewAdapter.MessageViewHolder> {

    private final ArrayList<ChatMessage> messages;

    public ChatRecyclerViewAdapter(ArrayList<ChatMessage> messages) {
        this.messages = messages;
    }


    @Override
    public MessageViewHolder onCreateViewHolder(ViewGroup parent,
                                                int viewType) {
        ItemMessageBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_message, parent, false);

        return new MessageViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MessageViewHolder holder, int position) {
        holder.setMessage(messages.get(position));
    }

    @Override
    public int getItemCount() {
        return messages.size();
    }

    public void addMessage(ChatMessage chatMessage) {
        messages.add(chatMessage);
        notifyItemInserted(messages.size());
    }

    public ArrayList<ChatMessage> getMessages() {
        return messages;
    }


    public static class MessageViewHolder extends RecyclerView.ViewHolder {

        private final ItemMessageBinding binding;

        MessageViewHolder(ItemMessageBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void setMessage(ChatMessage message) {
            binding.setMessage(message);
        }
    }
}
