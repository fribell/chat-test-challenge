package com.ribell.chatbot.ui.chat;

import android.text.TextUtils;

import com.ribell.chatbot.interfaces.OnCompletionListener;
import com.ribell.chatbot.model.ChatMessage;
import com.ribell.chatbot.repositories.MessageRepository;
import com.ribell.chatbot.repositories.MessageRepositoryApi;
import com.ribell.chatbot.repositories.SharedPreferencesRepository;
import com.ribell.chatbot.ui.core.BasePresenter;

import java.util.ArrayList;

public class ChatPresenter extends BasePresenter<ChatMvp.View> implements ChatMvp.Presenter {


    private static final String MY_PROFILE_IMAGE = "http://gravatar.com/avatar/fe89db8a9f9e400c57fe71bbb218449f/?s=120&d=mm";
    private final MessageRepository repository;
    private final SharedPreferencesRepository sharedPreferencesRepository;

    public ChatPresenter(MessageRepository repository, SharedPreferencesRepository sharedPreferencesRepository) {
        this.repository = repository;
        this.sharedPreferencesRepository = sharedPreferencesRepository;
    }

    @Override
    public void loadMessages() {
        // I assume that the server return the messages in the right order, if not we will have to order them by time.
        repository.getMessages(new OnCompletionListener<ArrayList<ChatMessage>>() {
            @Override
            public void onCompletionSuccess(ArrayList<ChatMessage> messages) {
                if (isViewAttached()) {
                    if (messages.size() > 0) {
                        view.displayMessages(messages);
                    } else {
                        view.displayEmptyMessages();
                    }
                }
            }

            @Override
            public void onCompletionError(int errorCode) {
                if (isViewAttached()) {
                    if (MessageRepositoryApi.ERROR_NO_INTERNET == errorCode) {
                        view.internetConnectionError();
                    } else {
                        view.unknownError();
                    }
                }
            }
        });
    }

    @Override
    public void sendMessage(String message) {
        // We should send that message to the server here but we don't have any API to do it

        if (!TextUtils.isEmpty(message)) {
            ChatMessage chatMessage = new ChatMessage(sharedPreferencesRepository.getUser().getName(),
                    message, MY_PROFILE_IMAGE, true);

            view.displayUserMessage(chatMessage);
        }
    }

    @Override
    public void logout() {
        // Here we should clean the database if we were doing offline
        sharedPreferencesRepository.removeUser();
        view.moveToLoginActivity();
    }
}
