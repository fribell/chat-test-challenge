package com.ribell.chatbot.ui.chat;

import com.ribell.chatbot.model.ChatMessage;
import com.ribell.chatbot.ui.core.BaseMvp;

import java.util.ArrayList;

public interface ChatMvp extends BaseMvp {
    interface View extends BaseView {
        void displayMessages(ArrayList<ChatMessage> arrayOfMessages);

        void displayEmptyMessages();

        void internetConnectionError();

        void unknownError();

        void displayUserMessage(ChatMessage chatMessage);

        void moveToLoginActivity();
    }

    interface Presenter extends BasePresenter<View> {
        void loadMessages();

        void sendMessage(String message);

        void logout();
    }
}
