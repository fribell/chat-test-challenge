package com.ribell.chatbot.repositories;

import android.content.SharedPreferences;

import com.ribell.chatbot.model.User;

public class SharedPreferencesRepositoryImpl implements SharedPreferencesRepository {

    public final static String TAG = SharedPreferencesRepositoryImpl.class.getSimpleName();
    private static final String USER_FULL_NAME = "USER_FULL_NAME";

    private final SharedPreferences sharedPreferences;

    public SharedPreferencesRepositoryImpl(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    @Override
    public void saveUser(User user) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(USER_FULL_NAME, user.getFullName());
        editor.apply();
    }

    @Override
    public boolean isLoggedIn() {
        return !sharedPreferences.getString(USER_FULL_NAME, "").isEmpty();
    }

    @Override
    public User getUser() {
        return new User(sharedPreferences.getString(USER_FULL_NAME, ""));
    }

    @Override
    public void removeUser() {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(USER_FULL_NAME);
        editor.apply();
    }
}
