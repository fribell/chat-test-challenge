package com.ribell.chatbot.repositories;

import com.ribell.chatbot.interfaces.OnCompletionListener;
import com.ribell.chatbot.model.ChatMessage;

import java.util.ArrayList;

public interface MessageRepository {

    void getMessages(OnCompletionListener<ArrayList<ChatMessage>> listener);
}
