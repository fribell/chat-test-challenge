package com.ribell.chatbot.repositories;

import com.ribell.chatbot.interfaces.OnCompletionListener;
import com.ribell.chatbot.model.ChatMessage;
import com.ribell.chatbot.net.ChatBotApiClient;
import com.ribell.chatbot.net.responses.ChatsResponse;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MessageRepositoryApi implements MessageRepository {

    public static final int ERROR_NO_INTERNET = 404;
    public static final int ERROR_UNKNOWN = 500;
    private final ChatBotApiClient chatBotApi;

    public MessageRepositoryApi(ChatBotApiClient chatBotApi) {
        this.chatBotApi = chatBotApi;
    }

    @Override
    public void getMessages(final OnCompletionListener<ArrayList<ChatMessage>> listener) {

        Call<ChatsResponse> getMessagesCall = chatBotApi.getMessageService().getMessages();
        getMessagesCall.enqueue(new Callback<ChatsResponse>() {
            @Override
            public void onResponse(Call<ChatsResponse> call, Response<ChatsResponse> response) {
                if (response.isSuccessful()) {
                    listener.onCompletionSuccess(response.body().getMessages());
                } else {
                    // We can handle better the errors to know exactly the reason and show the right message
                    listener.onCompletionError(ERROR_UNKNOWN);
                }
            }

            @Override
            public void onFailure(Call<ChatsResponse> call, Throwable t) {
                listener.onCompletionError(ERROR_NO_INTERNET);
            }
        });
    }
}
