package com.ribell.chatbot.repositories;

import com.ribell.chatbot.model.User;

public interface SharedPreferencesRepository {

    void saveUser(User fullName);

    boolean isLoggedIn();

    User getUser();

    void removeUser();
}
