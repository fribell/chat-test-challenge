package com.ribell.chatbot.model;

public class User {
    private String name;
    private String lastName;

    public User(String fullName) {
        String nameArray[] = fullName.split(" ");
        this.name = nameArray[0];
        this.lastName = nameArray.length > 1 ? nameArray[1] : "";
    }

    public String getLastName() {
        return lastName;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return name.concat(" ").concat(lastName);
    }
}
