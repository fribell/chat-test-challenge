package com.ribell.chatbot.model;

import android.text.format.DateFormat;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

public class ChatMessage implements Serializable {
    @SerializedName("username")
    private String username;

    @SerializedName("content")
    private String content;

    @SerializedName("userImage_url")
    private String profileImage;

    @SerializedName("time")
    private String time;

    private boolean deviceUser;

    public ChatMessage(String username, String content, String profileImage, boolean isDeviceUser) {
        this.username = username;
        this.content = content;
        this.profileImage = profileImage;

        DateFormat df = new DateFormat();
        this.time = df.format("hh:mm'h'", new Date()).toString();
        this.deviceUser = isDeviceUser;
    }

    public String getUsername() {
        return username;
    }

    public String getContent() {
        return content;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public String getTime() {
        return time;
    }

    public boolean isDeviceUser() {
        return deviceUser;
    }
}
